# RRZE CF7 Redirect

WP-Plugin: Erweiterung des Plugins "Contact Form 7", die eine Methode bereitstellt, um Besucher auf eine Erfolgsseite umzuleiten, wenn die Formulardaten erfolgreich übermittelt werden. Wenn keine Formulardaten per E-Mail gesendet werden oder wenn ein Fehler mit dem Formular auftritt, wird der Benutzer nicht umgeleitet.
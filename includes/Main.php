<?php

namespace RRZE\CF7Redirect;

defined('ABSPATH') || exit;

use RRZE\CF7Redirect\Redirect;

/**
 * [Main description]
 */
class Main
{
    /**
     * [protected description]
     * @var string
     */
    protected $cf7Plugin = 'contact-form-7/wp-contact-form-7.php';

    /**
     * [protected description]
     * @var boolean
     */
    protected $cf7PluginIsActive;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->cf7PluginIsActive = false;
    }

    /**
     * [onLoaded description]
     */
    public function onLoaded()
    {
        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        if (is_plugin_active($this->cf7Plugin)) {
            if (function_exists('wpcf7_add_form_tag')) {
                $this->cf7PluginIsActive = true;
            } elseif (current_user_can('activate_plugins')) {
                $this->pluginNoticeError(
                    __('The latest version of the plugin "Contact Form 7" is required for the "%s" plugin to work.', 'rrze-cf7-antispam')
                );
            }
        } elseif (current_user_can('activate_plugins')) {
            $this->pluginNoticeError(
                __('The plugin "Contact Form 7" must be installed and activated for the "%s" plugin to work.', 'rrze-cf7-antispam')
            );
        }

        if ($this->cf7PluginIsActive) {
            $redirect = new Redirect($this->cf7PluginIsActive);
            $redirect->onLoaded();
        }
    }

    /**
     * [pluginNoticeError description]
     * @param  string $message [description]
     */
    protected function pluginNoticeError(string $message = '')
    {
        $pluginData = get_plugin_data(plugin()->getFile());
        $pluginName = $pluginData['Name'];
        $tag = is_plugin_active_for_network(plugin()->getBaseName()) ? 'network_admin_notices' : 'admin_notices';
        add_action($tag, function () use ($message, $pluginName) {
            echo '<div class="notice notice-error"><p>';
            printf($message, $pluginName);
            echo '</p></div>';
        });
    }
}

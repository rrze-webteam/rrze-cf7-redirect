<?php

namespace RRZE\CF7Redirect;

defined('ABSPATH') || exit;

class Redirect
{
    /**
     * [protected description]
     * @var boolean
     */
    protected $cf7PluginIsActive;

    const redirectMetaKey = '_cf7_redirect_meta';

    const redirectFieldName = 'cf7_redirect_field_name';

    const redirectNonce = 'cf7_redirect_nonce';

    const redirectNonceField = 'cf7_redirect_nonce_field';

    /**
     * __construct
     * @param  boolean $cf7PluginIsActive
     */
    public function __construct(bool $cf7PluginIsActive = false)
    {
        $this->cf7PluginIsActive = $cf7PluginIsActive;
    }

    /**
     * onLoaded
     */
    public function onLoaded()
    {
        // CF7-JavaScript wird deaktiviert.
        add_filter('wpcf7_load_js', '__return_false');

        // Neuer Tab wird im CF7-Editor hinzugefügt.
        add_action('wpcf7_editor_panels', [$this, 'redirectPanel']);

        // Umleitungsseite-ID auslesen und die Post-Metadaten entsprechend aktualisieren.
        add_action('wpcf7_after_create', [$this, 'afterCreate']);

        // Post-Metadaten werden aktualisiert.
        add_action('wpcf7_after_save', [$this, 'afterSave']);

        // Der Benutzer wird umgeleitet, nachdem eine erfolgreiche E-Mail gesendet wird.
        add_action('wpcf7_mail_sent', [$this, 'mailSent'], 99);
    }

    /**
     * redirectPanel
     * Neuer Tab im CF7-Editor hinzufügen.
     * @param array $panels CF7 Panels
     * @return array Modified CF7 panels
     */
    public function redirectPanel(array $panels)
    {
        $panels['redirect-panel'] = [
            'title' => __('Redirect', 'rrze-cf7-redirect'), 
            'callback' => [$this, 'redirectPanelCallback']
        ];
        return $panels;
    }

    /**
     * redirectPanelCallback
     * Erstellt ein Feldset mit einer Dropdown-Liste von Seiten.
     * @param object $post
     */
    public function redirectPanelCallback(object $post)
    {
        wp_nonce_field(static::redirectNonceField, static::redirectNonce);
        $postMeta = get_post_meta($post->id(), static::redirectMetaKey, TRUE);
        $postId = absint($postMeta);

        $dropdown_options = [
            'echo' => 0,
            'name' => static::redirectFieldName,
            'show_option_none' => sprintf('&mdash; %s &mdash;', __('Select'), 'rrze-cf7-redirect'),
            'option_none_value' => '0',
            'selected' => $postId
        ];

        echo '<h3>' . __('Redirect', 'rrze-cf7-redirect') . '</h3>' . PHP_EOL;
        echo '<fieldset>' . PHP_EOL;
        echo '<legend>' . __('Select a page to redirect to on successful form submission.', 'rrze-cf7-redirect') . '</legend>' . PHP_EOL;
        echo wp_dropdown_pages($dropdown_options) . PHP_EOL;
        echo '</fieldset>' . PHP_EOL;
    }

    /**
     * afterCreate
     * Umleitungsseite-ID auslesen und die Post-Metadaten entsprechend aktualisieren.
     * @param object $contactForm
     * @return void
     */
    public function afterCreate($contactForm)
    {
        if (
            !isset($_REQUEST['post'])
            || !isset($_REQUEST['_wpnonce'])
            || !wpcf7_verify_nonce($_REQUEST['_wpnonce'])
        ) {
            return;
        }

        $contactFormId = $contactForm->id();

        $postId = absint($_REQUEST['post']);
        $postMeta = get_post_meta($postId, static::redirectMetaKey, TRUE);
        $postId = absint($postMeta);

        if (get_post_type($postId) == 'page' && get_post_status($postId) == 'publish') {
            update_post_meta($contactFormId, static::redirectMetaKey, $postId);
        }
    }

    /**
     * afterSave
     * Post-Metadaten aktualisieren.
     * @param object $contactForm
     * @return void
     */
    public function afterSave($contactForm)
    {
        if (
            !isset($_POST[static::redirectFieldName])
            || !isset($_POST[static::redirectNonce])
            || !wp_verify_nonce($_POST[static::redirectNonce], static::redirectNonceField)
        ) {
            return;
        }

        $contactFormId = $contactForm->id();

        $postId = absint($_POST[static::redirectFieldName]);

        if (get_post_type($postId) == 'page' && get_post_status($postId) == 'publish') {
            update_post_meta($contactFormId, static::redirectMetaKey, $postId);
        }
    }

    /**
     * mailSent
     * Der Benutzer wird umgeleitet, nachdem eine erfolgreiche E-Mail gesendet wird.
     * @param object $contactForm
     */
    public function mailSent($contactForm)
    {
        $contactFormId = $contactForm->id();
        $postMeta = get_post_meta($contactFormId, static::redirectMetaKey, TRUE);
        $postId = absint($postMeta);

        if (get_post_type($postId) == 'page' && get_post_status($postId) == 'publish') {
            wp_redirect(get_permalink($postId));
            exit;
        }
    }
}

��          t       �       �      �   �   �      �     �     �  ;   �  V   .  b   �      �  1   	  �  ;     8  6  P     �     �     �  c   �  g     s   }      �  1   	   Contact Form 7 Redirect Extension of the "Contact Form 7" plugin, which provides a method to redirect visitors to a success page if the form data is successfully submitted. If form data is not emailed or if the form fails, the user is not redirected. Plugins: %1$s: %2$s RRZE-Webteam Redirect Select a page to redirect to on successful form submission. The server is running PHP version %1$s. The Plugin requires at least PHP version %2$s. The server is running WordPress version %1$s. The Plugin requires at least WordPress version %2$s. https://blogs.fau.de/webworking/ https://github.com/RRZE-Webteam/rrze-cf7-redirect Project-Id-Version: RRZE Contact Form 7 Redirect
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-07-05 13:33+0000
PO-Revision-Date: 2020-07-05 13:40+0000
Last-Translator: RRZE Webteam <webmaster@fau.de>
Language-Team: Deutsch
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Loco https://localise.biz/
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
X-Loco-Version: 2.4.0; wp-5.4.2 Contact Form 7 Redirect Erweiterung des Plugins "Contact Form 7", die eine Methode bereitstellt, um Besucher auf eine Erfolgsseite umzuleiten, wenn die Formulardaten erfolgreich übermittelt werden. Wenn keine Formulardaten per E-Mail gesendet werden oder wenn ein Fehler mit dem Formular auftritt, wird der Benutzer nicht umgeleitet. Plugins: %1$s: %2$s RRZE-Webteam Umleiten Wählen Sie eine Seite aus, auf die bei erfolgreicher Formularübermittlung umgeleitet werden soll. Auf dem Server wird PHP-Version %1$s ausgeführt. Das Plugin benötigt mindestens die PHP-Version %2$s. Auf dem Server wird WordPress-Version %1$s ausgeführt. Das Plugin benötigt mindestens die WordPress-Version %2$s. https://blogs.fau.de/webworking/ https://github.com/RRZE-Webteam/rrze-cf7-redirect 